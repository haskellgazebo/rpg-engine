module ActionAST where

data Statement
  = Assign Name Expression
  | For Name Expression Block
  | While Expression Block
  | Exp Expression
  | If Expression Block
  | IfElse Expression Block Block
  | Import Name
  deriving (Show, Eq)

data Expression
  = Lit Int
  | Var Name
  | Negate Expression
  | Expression :+: Expression
  | Expression :-: Expression
  | Expression :*: Expression
  | Expression :/: Expression
  | Expression :<: Expression
  | Expression :>: Expression
  | Expression :~: Expression
  | Expression :&: Expression
  | Expression :|: Expression
  | Not Expression
  | Call Expression Args
  | Return Expression
  | List [Arg]
  deriving (Show, Eq)

data Arg = PosArg Expression deriving (Show, Eq)

type Name    = String
type Program = [Statement]
type Block   = [Statement]
type Args    = [Arg]