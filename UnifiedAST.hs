module UnifiedAST where

data Statement
  = NewRule Name Params Block
  | NewStrat Name Params Block
  | CStrat Name Block
  | Assign Name Expression
  | For Name Expression Block
  | While Expression Block
  | Exp Expression
  | If Expression Block
  | IfElse Expression Block Block
  | Import Name
  deriving (Show, Eq)

data Expression
  = Lit Int
  | Tr Bool
  | Var Name
  | LitStr String
  | Negate Expression
  | Expression :+: Expression
  | Expression :-: Expression
  | Expression :*: Expression
  | Expression :/: Expression
  | Expression :<: Expression
  | Expression :>: Expression
  | Expression :@: Expression
  | Expression :~: Expression
  | Expression :&: Expression
  | Expression :|: Expression
  | Expression :$: Expression
  | Not Expression
  | Call Expression Args
  | Return Expression
  | List [Arg]
  | Char Name Att
  deriving (Show, Eq)

data Arg
  = PosArg Expression
  deriving (Show, Eq)

type Name    = String
type Att     = String
type Params   = [String]
type Program = [Statement]
type Block   = [Statement]
type Args    = [Arg]