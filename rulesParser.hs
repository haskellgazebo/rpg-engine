module RulesParser where

import Text.ParserCombinators.Parsec
import Text.Parsec.Pos (newPos)
import Text.Parsec.Expr 
import Control.Applicative ((*>), (<*))
import Data.Char
import Data.Either
import UnifiedAST

type EParser a = GenParser Char () a
type LParser a = GenParser Lex () a

data Lex = Ident String | Num String | Op String | Indent Int | End | Plit String
	deriving Show

parseIdent :: EParser Lex
parseIdent = do
				spaces
				s1 <- letter
				s2 <- many (letter <|> digit)
				spaces
				return $ Ident (s1:s2)

parseNum :: EParser Lex
parseNum = do
				spaces
				n <- many1 digit
				spaces
				return $ Num n

parseIndent :: EParser Lex
parseIndent = do
				x <- many (Text.ParserCombinators.Parsec.space)
				return $ Indent $ length x
				
parseOp :: EParser Lex
parseOp =  do 
				spaces
				x <- oneOf "+-*/=:(),<>.~@$&|"
				spaces
				return $ Op [x]		

plit :: EParser Lex
plit = do char '"'
          a <- manyTill (anyChar) (char '"')
          return $ Plit a

parseToken :: EParser Lex
parseToken =   try parseIdent <|> try parseNum <|> try plit <|> parseOp

parseLine :: EParser [Lex]
parseLine = do
				x <- parseIndent
				y <- many parseToken
				return $ x : y

--add the End tokens to the array of lexs
addEndLine :: [Lex] -> [Int] -> [Lex]
addEndLine [] [] = []
addEndLine [] list = End : addEndLine [] (init list)
addEndLine ((Op ":"):(Indent x):xs) list = (Op ":") : addEndLine xs (list++[x])
addEndLine (Indent x:Indent y:xs) list = addEndLine (Indent y:xs) list
addEndLine (Indent x:xs) list 	| x > last list = addEndLine xs (list)
								| x == last list = End : addEndLine xs list
								| x < last list = End : addEndLine (Indent x:xs) (init list)
addEndLine (x:xs) list = x : addEndLine xs list

--function to run the lexxer
runLex "" = []
runLex input = addEndLine (tail $ concat $ rights $ map (parse parseLine "?") (lines input)) [0]

isTok :: (Lex -> Bool) -> LParser Lex
isTok f = token show (\t -> newPos "" 0 0) 
                (\t -> if f t then Just t else Nothing)

isNum :: Lex -> Bool
isNum (Num _) = True
isNum _		  = False

isIdent :: Lex -> Bool
isIdent (Ident _) = True
isIdent _		  = False

isOp :: Lex -> Bool
isOp (Op "+") = True
isOp (Op "-") = True
isOp (Op "*") = True
isOp (Op "/") = True
isOp (Op "<") = True
isOp (Op ">") = True
isOp _		= False

isOpen :: Lex -> Bool
isOpen (Op "(") = True
isOpen _		= False

isClose :: Lex -> Bool
isClose (Op ")") = True
isClose _		 = False

isEquals :: Lex -> Bool
isEquals (Op "=") = True
isEquals _ 		  = False

isComma :: Lex -> Bool
isComma (Op ",") = True
isComma _ 		= False

isColon :: Lex -> Bool
isColon (Op ":") = True
isColon _		 = False

isFor :: Lex -> Bool
isFor (Ident "for") = True
isFor _				= False

isIn :: Lex -> Bool
isIn (Ident "in") = True
isIn _			  = False

isIf :: Lex -> Bool
isIf (Ident "if") = True
isIf _			  = False

isElse :: Lex -> Bool
isElse (Ident "else") = True
isElse _			  = False

isEnd :: Lex -> Bool
isEnd End = True
isEnd _ = False

isMult :: Lex -> Bool
isMult (Op "*") = True
isMult _		= False

isDiv :: Lex -> Bool
isDiv (Op "/") = True
isDiv _		   = False

isPlus :: Lex -> Bool
isPlus (Op "+") = True
isPlus _		= False

isMinus :: Lex -> Bool
isMinus (Op "-") = True
isMinus _		 = False

isLess :: Lex -> Bool
isLess (Op "<") = True
isLess _		= False

isGreater :: Lex -> Bool
isGreater (Op ">") = True
isGreater _		   = False

isReturn :: Lex -> Bool
isReturn (Ident "return") = True
isReturn _					= False

isPeriod :: Lex -> Bool
isPeriod (Op ".") = True
isPeriod _ 		  = False

isTilda :: Lex -> Bool
isTilda (Op "~") = True
isTilda _		 = False

isDollars :: Lex -> Bool
isDollars (Op "$") = True
isDollars _		   = False

isAt :: Lex -> Bool
isAt (Op "@") = True
isAt _		  = False

isAnd :: Lex -> Bool
isAnd (Op "&") = True
isAnd _		   = False

isBar :: Lex -> Bool
isBar (Op "|") = True
isBar _ 	   = False

isImport :: Lex -> Bool
isImport (Ident "import") = True
isImport _				  = False

isPlit :: Lex -> Bool
isPlit (Plit _) = True
isPlit _		= False

--program
--	::= stmt*
program :: LParser [Statement]
program = endBy stmt (isTok isEnd) -- <* eof

--stmt
--	::=   1 -> ident '=' expr
--		| 2 -> 'for' ident 'in' expr ':' block
--		| 3 -> expr
--		| 4 & 5 -> if x<2:
--  			 a = 2
--			   else:
--   			 y = 2
--		| 6 -> NewRule
--		| 7 -> Import

stmt1 = do
			(Ident i) <- isTok isIdent 
			isTok isEquals
			e <- expr
			return (Assign i e)
			
stmt2 = do
			isTok isFor
			(Ident n) <- isTok isIdent
			isTok isIn
			e <- expr
			isTok isColon
			b <- block
			return (For n e b)
stmt3 = do
			e <- expr
			return (Exp e)

-- just the if statement and a block of code
stmt4 = do
			isTok isIf
			e <- expr
			isTok isColon
			b <- block
			return (If e b)

--the ifelse statement and two blocks of code
stmt5 = do
			isTok isIf
			e <- expr
			isTok isColon
			b1 <- block
			isTok isEnd
			isTok isElse
			isTok isColon
			b2 <- block
			return (IfElse e b1 b2)
stmt6 = do 
			(Ident name) <- isTok isIdent
			isTok isOpen
			p <- paramlist
			isTok isClose
			isTok isColon
			b <- block
			return (NewRule name p b)
stmt7 = do
			isTok isImport
			(Ident p1) <- isTok isIdent
			isTok isPeriod
			(Ident p2) <- isTok isIdent
			return (Import (p1++"."++p2))

param = do
		(Ident n) <- isTok isIdent
		return n
paramlist = sepBy param (isTok isComma)
			
stmt :: LParser Statement
stmt = try stmt5 <|> try stmt6 <|> try stmt4 <|> try stmt2 <|> try stmt1 <|> try stmt7 <|> stmt3

--block
--	::= stmt*
block :: LParser [Statement]
block = endBy stmt (isTok isEnd)

--expr
--	::=   1 -> expr op expr
--		| 2 -> expr '(' arglist ')'
--		| int
--		| ident
--		| 3 -> '(' expr ')'
expr1 :: LParser Expression
{-
expr1 = do
			e1 <- (try expr2 <|> try expr3 <|> try exprInt <|> exprIdent)
			o <- op
			e2 <- expr
			case o of
				"+"	-> return (e1 :+: e2)
				"-"	-> return (e1 :-: e2)
				"*"	-> return (e1 :*: e2)
				"/"	-> return (e1 :/: e2)
				"<" -> return (e1 :<: e2)
				">" -> return (e1 :>: e2)
-}
expr1 = buildExpressionParser table term <?> "Expression"

table = [[punct "*" (:*:) AssocLeft, punct "/" (:/:) AssocLeft, punct "$" (:$:) AssocLeft]
        ,[punct "+" (:+:) AssocLeft, punct "-" (:-:) AssocLeft, punct "~" (:~:) AssocLeft, punct "@" (:@:) AssocLeft, punct "&" (:&:) AssocLeft,punct "|" (:|:) AssocLeft, prefix "-" Negate]
        ,[punct "<" (:<:) AssocNone, punct ">" (:>:) AssocNone]
        ]          
punct s f assoc = Infix (do{ opr s >> return f}) assoc
prefix s f = Prefix (do{ opr s >> return f })

opr s = case s of
	"*" -> isTok isMult
	"/" -> isTok isDiv
	"+" -> isTok isPlus
	"-" -> isTok isMinus
	"<" -> isTok isLess
	">" -> isTok isGreater
	"~" -> isTok isTilda
	"$" -> isTok isDollars
	"@" -> isTok isAt
	"&" -> isTok isAnd
	"|" -> isTok isBar

term = try exprChar <|> try expr2 <|> try expr3 <|> try exprInt <|> try exprString  <|> exprIdent <?> "Simple Expression"
				
expr2 :: LParser Expression
expr2 = do
			e1 <- (try expr3 <|> try exprInt <|> try exprIdent)
			isTok isOpen
			a <- arglist
			isTok isClose
			return (Call e1 a)
			
expr3 :: LParser Expression
expr3 = do
			isTok isOpen
			e <- (expr)
			isTok isClose
			return e

exprInt :: LParser Expression
exprInt = do
			(Num x) <- isTok isNum
			return $ Lit (read x::Int)
			
exprIdent :: LParser Expression
exprIdent = do
			(Ident x) <- isTok isIdent 
			return $ Var x

exprReturn :: LParser Expression
exprReturn = do
			isTok isReturn
			e <- expr
			return (Return e)
exprChar :: LParser Expression
exprChar =	do
			(Ident name) <- isTok isIdent
			isTok isPeriod
			(Ident att) <- isTok isIdent
			return (Char name att)

exprString :: LParser Expression
exprString = do
			(Plit s) <- isTok isPlit
			return (LitStr s)
expr :: LParser Expression
expr = try exprReturn  <|> try exprString <|> try expr1 <|> try exprChar <|> try expr2 <|> try expr3 <|> try exprInt <|> try exprIdent
--expr = exprIdent

--op
--	::= '*' | '/' | '+' | '-' | '<' | '>'
op :: LParser String
op = do
		(Op x) <- isTok isOp
		return x

--arglist
--	::= arg ',' ...
arglist = sepBy arg (isTok isComma) 

--arg
--	::= expr | ident '=' expr
arg :: LParser Arg
arg1 = do
			e <- expr
			return (PosArg e)
--arg2 = do
--			(Ident name) <- isTok isIdent
--			isTok isEquals
--			e <- expr
--			return (NamedArg name e)
--arg = try arg2 <|> arg1
arg = arg1

--function to run the final parser
getStmts :: Either ParseError [Statement] -> [Statement]
getStmts (Right xs) = xs

parseRules :: String -> [Statement]
parseRules input = getStmts $ parse program "error" (runLex input)

rulesParseTest p inp = let a = runLex inp in parseTest p a