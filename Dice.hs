module Dice where
import System.Random

type Die = Int

d :: Int -> Int -> IO Int
d a b = do gen <- newStdGen
           if a < 1 
           then error "Invalid number of dice. (must be >= 1)"
           else if b < 1 
                then error "Invalid number of sides for a dice. (must be >= 1)"
                else do let lst = (take a) $ (randomRs (1, b) gen)
                        return $ foldl (+) 0 lst
           
           