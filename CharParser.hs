module CharParser where
import CharAST
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.Parsec.Pos (newPos)
import Debug.Trace
import Data.List

data Lex = PName String | PNum String | Operator Char | Indent Int | End
     deriving (Show, Eq)

type LParser a = GenParser Lex () a

-- Parser code:

charFile :: LParser CList
charFile = do a <- many (try actor)
              b <- many (try (ctype a))
              c <- many (character b)
              return $ foldl (++) [] c

character :: [CharacterType] -> LParser CList
character lst = do (PName chartype) <- isToken $ isName
                   let chtype = filter (\(CharacterType nam1 _ _) -> nam1 == chartype) lst
                   case chtype of [s@(CharacterType _ _ atts)] -> do charnames <-  sepBy (isToken $ isName) (isToken $ isOp ',')
                                                                     isToken $ isEnd
                                                                     return $ map (\(PName a) -> Character a s atts) charnames
                                  _                            -> error "CharacterType has been declared that does not exist."

ctype :: [ActorType] -> LParser CharacterType
ctype lst = do (PName act) <- isToken $ isName
               (PName name) <- isToken $ isName
               isToken $ isOp ':'
               atts <- many1 chatt
               isToken $ isEnd
               let vals = filter (\(ActorType nam1 _) -> nam1 == act) lst
               case vals of [x] -> let (ActorType _ attnames) = x 
                                   in if length (filter (\(attname, _) -> elem attname attnames) atts) == length atts && length atts == length attnames
                                      then return $ CharacterType name x atts
                                      else error "Incorrect amount of attributes"
                            _   -> error "Declared CharacterType whose ActorType has not been declared."

chatt :: LParser (String, Int)
chatt = do (PName name) <- isToken $ isName
           isToken $ isOp '='
           (PNum val) <- isToken $ isNum
           isToken $ isEnd
           return (name, toInt val)

actor :: LParser ActorType
actor = do isToken $ isWord "ActorType"
           (PName name) <- isToken $ isName
           isToken $ isOp ':'
           atts <- many1 att
           isToken $ isEnd
           return $ ActorType name atts

att :: LParser String
att = do (PName a) <- isToken $ isName
         isToken $ isEnd
         return a

-- Dr. Peterson's isTok method, changed the name so I could import his and my code into GHCi w.o conflicts.
isToken :: (Lex -> Bool) -> LParser Lex
isToken f = token show (\t -> newPos "" 0 0) 
                (\t -> if f t then Just t else Nothing)

isWord :: String -> Lex -> Bool
isWord word (PName wd) = word == wd
isWord _ _ = False

isOp :: Char -> Lex -> Bool
isOp ch (Operator ch1) = ch == ch1
isOp _ _ = False

isName :: Lex -> Bool
isName (PName _) = True
isName _        = False

isNum :: Lex -> Bool
isNum (PNum _) = True
isNum _        = False

isEnd :: Lex -> Bool
isEnd (End) = True
isEnd _ = False

toInt :: String -> Int
toInt str = read str

getCharacter :: Either ParseError CList -> CList
getCharacter (Right xs) = xs

parseIt :: Show a => LParser a -> String -> IO ()
parseIt p inp = let (Right x) = parse srcFile "?" inp in parseTest p x

parseChar :: String -> CList
parseChar inp = let (Right x) = parse srcFile "?" inp in
  getCharacter $ parse charFile "?" x

-- Lexer code:

srcFile :: GenParser Char st [Lex]
srcFile = do a <- sepBy line eol
             return $ process a

line :: GenParser Char st [Lex]
line = do a <- indent
          b <- many toke
          return (a:b)

process :: [[Lex]] -> [Lex]
process a = convertToEnds $ removeBlLi a

removeBlLi :: [[Lex]] -> [[Lex]]
removeBlLi a = [x | x <- a, case x of [Indent _] -> False
                                      _          -> True]

pname :: GenParser Char st Lex
pname = do a <- letter
           b <- many (letter <|> digit)
           return $ PName (a : b)

num :: GenParser Char st Lex
num = do a <- many1 digit
         return $ PNum a

oper :: GenParser Char st Lex
oper = do a <- oneOf "=:,"
          return $ Operator a

toke :: GenParser Char st Lex
toke =  do a <- try oper <|> try num <|> pname
           skipMany $ char ' '
           return a

indent :: GenParser Char st Lex
indent = try tabs <|> indents

indents :: GenParser Char st Lex
indents = do a <- many $ char ' '
             return $ (Indent $ length a)

tabs :: GenParser Char st Lex
tabs = do a <- many1 tab
          return $ (Indent $ length a)

convertToEnds :: [[Lex]] -> [Lex]
convertToEnds lst = convertToEndsTup lst (negate 1) []

convertToEndsTup :: [[Lex]] -> Int -> [Int] -> [Lex]
convertToEndsTup [] lvl fors = []
convertToEndsTup [x] _ _    =         tail x ++ [End]
convertToEndsTup (x:xs) lvl fors =         let (Indent indOne) = head x
                                               (Indent indTwo) = head $ head xs
                                               noInd = tail x
                                               lvl1 = if lvl == -1 then indOne else lvl
                                           in if (indTwo > indOne)
                                              then if last x == Operator ':'
                                                   then let (blk, rst) = getBlock indOne xs in tail x ++ convertToEndsTup blk (negate 1) [] ++ (End : convertToEndsTup rst lvl1 fors) --- call this on all following lines farther than this for, stop when indentation <= this statement
                                                   else tail x ++ convertToEndsTup xs lvl1 fors
                                              else if (indTwo == indOne)
                                                   then if (lvl1 /= indOne)
                                                        then tail x ++ convertToEndsTup xs lvl1 fors
                                                        else tail x ++ (End : convertToEndsTup xs lvl1 fors)
                                                   else if (indTwo > lvl1)
                                                        then tail x ++ convertToEndsTup xs lvl1 fors
                                                        else if (not $ null $ fors)
                                                             then if (indTwo <= head fors)
                                                                  then tail x ++ (getEnds fors indTwo) ++ (convertToEndsTup xs (negate 1) (dropWhile (>= indTwo) fors))
                                                                  else undefined
                                                             else tail x ++ (End : convertToEndsTup xs (negate 1) fors)

getBlock :: Int -> [[Lex]] -> ([[Lex]], [[Lex]])
getBlock ind lst = (takeWhile (\x -> let (Indent i) = head x in i > ind) lst, dropWhile (\x -> let (Indent i) = head x in i > ind) lst)

getEnds :: [Int] -> Int -> [Lex]
getEnds fors ind = let oldFors = takeWhile (>= ind) (fors)
                       in replicate (length oldFors + 1) (End)

-- EOL def taken from Real World Haskell. Figured I'd put it in since I'm coding on a Windows machine.
eol =   try (string "\n\r")
    <|> try (string "\r\n")
    <|> string "\n"
    <|> string "\r"

-- Test Functions:

testLex :: String -> IO ()
testLex inp = parseTest srcFile inp

testLexF inp = do (Right a) <- parseFromFile srcFile inp
                  return a

charParseTest p inp = let (Right a) = parse srcFile "?" inp in parseTest p a

charParseTestF p file = do (Right a) <- parseFromFile srcFile file
                           parseTest p a