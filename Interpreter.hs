module Interpreter where

import CharParser
import RulesParser
import StratParser
import ActionParser
import UnifiedAST
import CharAST
--import Graphics.UI.SDL
import Dice

import Control.Monad.State
import Data.List
import System.Random
--import Data.IO


data RpgEnv	= RpgEnv 	{ rules :: [(String, Lambda)]
						, charList :: [Character]
						, stratList :: [Stratc]
						, vars :: [(String, Var)]
						, lists :: [(String, Var)]
						, tempVars :: [(String, Var)]}
                        
defaultRpg :: RpgEnv
defaultRpg = RpgEnv 	{ rules = [] :: [(String, Lambda)]
						, charList = [] :: [Character]
						, stratList = [] :: [Stratc]
						, vars = [] :: [(String, Var)]
						, lists = [] :: [(String, Var)]
						, tempVars = [] :: [(String, Var)]}

--Rules list holds the name of the rule and then the function

data Lambda		=	Lambda	Params Block
--data Slambda	=	Slambda	Args Block 
data Stratc	=	Stratc	{ char:: String
						, stratN :: String
						, strategy :: Lambda }

data Var = Numb Int | Truth Bool | Str String | Lst [String] deriving (Show)

--data Rule 	= 	TF ([Character]->Bool)
--		  	| 	Disp ([Character]->IO ())

runTest :: IO ()
runTest = run "smallgame.chr" "smallgame.rul" "smallgame.strat" "smallgame.act"

run :: String -> String -> String -> String -> IO ()
run charfile rulefile stratfile actionfile = evalStateT (runPrgm charfile rulefile stratfile actionfile) defaultRpg

runPrgm :: String -> String -> String -> String -> StateT (RpgEnv) IO ()
runPrgm charfile rulefile stratfile actionfile =    do
                                                    cfile <- lift $ readFile charfile
                                                    rfile <- lift $ readFile rulefile
                                                    sfile <- lift $ readFile stratfile
                                                    afile <- lift $ readFile actionfile
                                                    let aparsed = parseActions afile
                                                    let rparsed = parseRules rfile
                                                    let cparsed = parseChar cfile
                                                    let sparsed = parseStrat sfile
                                                    inCharacter cparsed
                                                    inRules rparsed
                                                    inStrat sparsed
                                                    a <- doIt aparsed
                                                    lift $ putStrLn (show a)

isCharacter :: Character -> StateT RpgEnv IO ()
isCharacter (Character cname ctype cattributes)	= do
												  env <- get
												  if ( 0 < (length $ getName (charList env) cname))
												  		then error("Character already exists: " ++ cname)
												  		else put RpgEnv { rules = rules env , charList = (Character cname ctype cattributes):(charList env), stratList = stratList env, vars = vars env, lists = lists env, tempVars = tempVars env}
--isCharacter _ 									= return ()

isRule :: Statement -> StateT RpgEnv IO ()
isRule	(NewRule name param b)	=	do
									env <- get
									if any ((==) name) (map fst $ rules env)
										then error("Only one rule for each name: " ++ name)
										else put RpgEnv { rules = (name, Lambda param b):(rules env) , charList= charList env, stratList= stratList env, vars = vars env, lists = lists env, tempVars = tempVars env}
isRule	_						=	return ()

isCStrat :: Statement -> StateT RpgEnv IO [()]
isCStrat	(CStrat name b)		=	mapM (isStrat name) b
isCStrat	_					=	return [()]

isStrat :: String -> Statement -> StateT RpgEnv IO ()
isStrat n (NewStrat name p b)	=	do
									env <- get
									let sl = stratList env in
										case filter (\x -> (char x) == n) sl of
											[]	->	put RpgEnv {rules = rules env, charList = charList env, stratList = (Stratc{char=n, stratN=name, strategy=(Lambda p b)}):(stratList env), vars = vars env, lists= lists env, tempVars = tempVars env}
											s	->	if any (\x -> (stratN x)==name) s
														then error("Only one strategy per char per type: " ++ name)
														else put RpgEnv { rules = rules env, charList = charList env, stratList= (Stratc{char=n, stratN=name, strategy=(Lambda p b)}):(stratList env), vars = vars env, lists =lists env, tempVars = tempVars env }

isAction :: Statement	-> StateT RpgEnv IO (Maybe Bool)
isAction	(Assign n e)	=	do
								env <- get
								l 	<- isExpression e
								case l of
										Lst k	-> put RpgEnv {rules = rules env, charList = charList env, stratList = stratList env, vars = vars env, lists = (n, Lst k):(lists env), tempVars = tempVars env}
										_		-> put RpgEnv {rules = rules env, charList = charList env, stratList = stratList env, vars = (n, l):(vars env), lists = lists env, tempVars = tempVars env} 
								return Nothing
isAction	(For n e b)		= 	do
								env <- get
								t <- isExpression e
								case t of
									Lst s	->  mapM (\x ->	do
															put RpgEnv {rules = rules env, charList = charList env, stratList = stratList env, vars = vars env, lists = lists env, tempVars = (n,  Str x):(tempVars env)}
															doIt b
															put RpgEnv {rules = rules env, charList = charList env, stratList = stratList env, vars = vars env, lists = lists env, tempVars = drop 1 (tempVars env)}) s
									_		-> error("ForEach must be iterable")
								return Nothing
isAction	(While e b)		=	do
								t <- isExpression e
								case t of
									Truth True  -> doIt b
									Truth False -> return Nothing
									_			-> error ("While expression must be boolean")
								isAction (While e b)
isAction	(Exp e)			=	case e of
									Return e2 	-> 	do
													t <- isExpression e2
													return (case t of
																Truth True  -> Just True
																Truth False -> Just False
																_			-> error ("Must return bool" ++ show t))
									_			->	do
													--lift $ putStrLn $ show e
													r <- isExpression e
													return (case r of
																Truth gl 	-> Just gl
																_			-> Nothing)
isAction	(If e b)		=	do
								t <- isExpression e
								case t of
									Truth True ->	do
													p <- doIt b
													return (case p of
																Just gl	-> Just gl
																_		-> Nothing)
									Truth False -> return Nothing
									_			-> error("If must evaluate to boolean")
isAction	(IfElse e b c)	=	do
								t <- isExpression e
								case t of
									Truth True ->	do
													p <- doIt b
													return (case p of
																Just gl	-> Just gl
																_		-> Nothing)
									Truth False ->  do
													q <- doIt c
													return (case q of
																Just yl	-> Just yl
																_		-> Nothing)
									_			-> error("If must evaluate to boolean")
isAction	(Import n)		=	return Nothing
isAction	_				=	error("Statement Invalid.")

isExpression :: Expression -> StateT RpgEnv IO Var
isExpression	(Lit i)		=	return (Numb i)
isExpression	(Tr b)		=	return (Truth b)
isExpression	(Var n)		=	do
								env <- get
								return	(case (find (\x -> (fst x) ==n ) (tempVars env)) of
											Just w 	->	(snd w)
											Nothing	->	case (find (\x -> (fst x) == n) (vars env)) of
															Just t	->	(snd t)
															Nothing	->	case (find (\x -> (fst x) == n) (lists env)) of
																			Just p	-> (snd p)
																			Nothing	-> error("Invalid variable: " ++ n))
isExpression	(LitStr n)	=	return (Str n)
isExpression	(Negate e)	=	do
								g <- isExpression e
								return 	(case g of
											Numb i		-> (Numb (-i))
											Truth True	-> (Truth False)
											Truth False	-> (Truth True)
											_			-> error("YOU CANNOT NEGATE THAT"))
isExpression	(e1 :+: e2)	=	do
								p <- isExpression e1
								q <- isExpression e2
								return	(case p of
											Numb i	->	case q of
															Numb j	->	Numb (i+j)
															Str  t 	-> 	Str((show i) ++ t)
															_		->	error("Incompatible add")
											Str s	->	case q of
															Str t	->	Str (s ++ t)
															q		->	Str (s ++ (show q))
											_		-> error("Incompatible add"))
isExpression	(e1 :-: e2)	=	do
								p <- isExpression e1
								q <- isExpression e2
								return	(case p of
											Numb i	->	case q of
															Numb j	->	Numb (i-j)
															_		->	error("Incompatible subtract")
											_		-> error("Incompatible subtract"))
isExpression	(e1 :*: e2)	=	do
								p <- isExpression e1
								q <- isExpression e2
								return	(case p of
											Numb i	->	case q of
															Numb j	->	Numb (i*j)
															_		->	error("Incompatible multiply")
											_		-> error("Incompatible multiply"))
isExpression	(e1 :/: e2)	=	do
								p <- isExpression e1
								q <- isExpression e2
								return	(case p of
											--Numb i	->	case q of
											--				Numb j	->	Numb (i/j)
											--				_		->	error("Incompatible divide")
											_		-> error("Divides are for fools"))
isExpression	(e1 :<: e2)	=	do
								p <- isExpression e1
								q <- isExpression e2
								return	(case p of
											Numb i	->	case q of
															Numb j	->	Truth (i<j)
															_		->	error("Incompatible less than")
											_		-> error("Incompatible less than"))
isExpression	(e1 :>: e2)	=	do
								p <- isExpression e1
								q <- isExpression e2
								return	(case p of
											Numb i	->	case q of
															Numb j	->	Truth (i>j)
															_		->	error("Incompatible less than")
											_		-> error("Incompatible less than"))
isExpression	(e1 :~: e2)	=	do
								p <- isExpression e1
								q <- isExpression e2
								return	(case p of
											Numb i	->	case q of
															Numb j	->	Truth (i==j)
															_		->	error("Incompatible equivalence")
											Truth b	->	case q of
															Truth c	->	Truth(b==c)
															_		->	error("Incompatible equivalence")	
											_		-> error("Incompatible equivalence"))
isExpression 	(e1 :@: e2)	=	do
								q <- isExpression e2
								case e1 of
									Char n a	->	case q of
														Numb j	->	do 
																	env <-	get
																	let l = (case head $ getName (charList env) n of
																				Character n ct as 	-> Character n ct ((a, k):as) where k = (case (find (\x -> (fst x) == a) as) of
																																				Just m	->	(snd m) + j
																																				Nothing	->	error("YOU FAILED: " ++ a))) in
																		put RpgEnv { rules = rules env, charList = l:(getOtherName (charList env) n), stratList= stratList env, vars = vars env, lists = lists env, tempVars = tempVars env}
																	return (Truth True)
														_		->	return $ error("Incompatible mod: "++n)	
									_			-> return $ error("Incompatible mod (not char)")
isExpression	(e1 :&: e2)	=	do
								p <- isExpression e1
								q <- isExpression e2
								return	(case p of
											Truth b	->	case q of
															Truth c	->	Truth (b && c)
															_		->	error("Incompatible &&")	
											_		-> error("Incompatible &&"))
isExpression	(e1 :|: e2)	=	do
								p <- isExpression e1
								q <- isExpression e2
								return	(case p of
											Truth b	->	case q of
															Truth c	->	Truth (b || c)
															_		->	error("Incompatible ||")	
											_		-> error("Incompatible ||"))
isExpression	(Not e)		=	do
								p <- isExpression e
								return	(case p of
												Truth False	-> Truth True
												Truth True	-> Truth False
												_			-> error ("Bad not"))


--STRATEGIES HAVE A VERY FIXED DOMAIN 
isExpression	(Call e a)	=	do
								env	<-	get
								case e of
									Var n	->	do
												def <- preDef n a
												case def of
													Just hb -> 	return hb
													Nothing ->	case filter (\x -> (stratN x) == n) (stratList env) of
																	[]	->	case find (\x -> (fst x) == n ) (rules env) of
																				Just po 	->	do
																							l	<- let lp = (map (\x -> case x of
																															PosArg eh -> eh) a) in
																										mapM isExpression lp
																							to	<- doItWReplace (snd po) l
																							return (case to of
																										Just bn -> Truth bn
																										Nothing -> Truth True)
																				Nothing		-> error ("Bad call: " ++ n)
																	sts ->	case head a of
																				PosArg z	->	case z of
																									LitStr yo 	->	case find (\x -> (char x) == yo) sts of
																														Just st	->	do
																															--AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
																																	g<-doStrat (strategy st) yo 
																																	return ( case g of
																																				Just q	-> Truth q
																																				Nothing -> Truth True)
			
																														Nothing ->	error ("Bad name- This is where input handling would go")
																									Var yn 		->	case find (\x -> (fst x) == yn) (tempVars env) of
																														Just (ls, Str qw) -> case find (\x -> (char x) == qw) sts of
																																				Just st	->	do
																																							g<-doStrat (strategy st) yn 
																																							return ( case g of
																																										Just q	-> Truth q
																																										Nothing -> Truth True)
																																				Nothing ->	error ("Bad name - " ++ qw ++ " " ++ ls)
																														Nothing ->	error ("Bad name- This is where input handling would go: " ++ yn)
																									_			->	error ("Bad strat call (nonstring arg): " ++ n)
																				--_		-> error ("Wrong")
									_		->	error ("YOU ARE A STUPID IDIOT")
isExpression	(Return e)	=	do
								t <- isExpression e
								return t

isExpression	(List as)	=	return (Lst (map (\x -> case x of
															PosArg t -> case t of
																			LitStr s 	-> s
																			_		 	-> error ("Only list strings")) as))
isExpression	(Char n a)	=	do
								env <- get
								return (case (find (\x -> (fst x) == n) (tempVars env)) of
											Just lm ->	case snd lm of
															Str rt 	->	case (find (\x -> (fst x) == (rt)) (map (\x -> case x of
																														Character s ct ats -> (s, ats)) (charList env))) of
																			Just ph	->	case find (\x -> a == (fst x)) (snd ph) of
																							Just k	-> Numb (snd k)
																							Nothing	-> error ("Not an attribute:" ++ a) 
																			Nothing ->	error("Bad temp var: " ++ (rt))
											Nothing	->	case (find (\x -> (fst x) == n) (map (\x -> case x of
																										Character s ct ats -> (s, ats)) (charList env))) of
															Just t 	->	case find (\x -> a == (fst x)) (snd t) of
																			Just k	-> Numb (snd k)
																			Nothing	-> error ("Not an attribute:" ++ a) 
															Nothing ->	error("Not a character: " ++ n))
isExpression	(e1 :$: e2)	=	do
								p	<- isExpression e1
								q	<- isExpression e2
								case p of
									Numb i 	-> case q of
													Numb j	-> 	do
																t <- lift $ d i j
																return (Numb t)
													_		-> 	return $ error("Must roll a number sided die")
									_		-> return $ error ("Must roll a number")

--isExpression	_		=	error("Expression Invalid.")
--intRule :: Args->Block->Lambda
--intRule p b	=	error("Derp--inRules :: Statement -> State RpgEnv ()
preDef :: String -> Args -> StateT RpgEnv IO (Maybe Var)
preDef	n  a	|	n=="empty"	=	do
									env <- get
									case head a of
										PosArg z -> case z of
														Var n -> case find (\x -> n== (fst x)) (lists env) of
																		(Just tip) -> case snd tip of
																						Lst pq	->	if length pq == 0
																										then return (Just (Truth True))
																										else return (Just (Truth False))
																						_		-> error("not a list")
																		Nothing-> error("Must call empty on a list")
										--_	  -> error("Bad empty expression")
				|	n=="get"	=	do
									env <- get
									case head a of
										PosArg z -> case z of
														Var n 	-> case find (\x -> n== (fst x)) (lists env) of
																			(Just tip)	-> case snd tip of
																							Lst pq	-> case (a !! 1) of
																											PosArg yx -> case yx of
																															Lit i	-> return (Just (Str(pq !!i)))
																															_		-> error ("Index by number")
																							_		-> error("Get is on lists")
																			Nothing -> error ("Must call get on a list: " ++ n)
														_		-> error ("You did not pass a var")
				| 	n=="info"	=	case head a of
										PosArg z	->	do
														t <- isExpression z
														case t of
															Str s -> lift $ putStrLn $ show s
															Numb i-> lift $ putStrLn $ show i
															_	  -> lift $ putStrLn $ show t
														
														--case t of
														--	Str s 	-> lift $ putStrLn s
														--	Numb i -> lift $ putStrLn $ show i
														--	_			-> error ("cannot print that")
														--lift $ putStrLn t
														return (Just (Truth True))
				|	n=="attempt"=	case head a of
										PosArg	z 	->  do
														lift $ putStrLn "Attempt"
														t <- isExpression z
														case t of 
															Truth True ->	case (a!!1) of
																				PosArg jb	->	do
																								f <- isExpression jb
																								lift $ putStrLn $ show f
																								return (Just (Truth True))
															Truth False ->	case (a!!2) of
																				PosArg jb	->	do
																								f <- isExpression jb
																								lift $ putStrLn $ show f
																								return (Just (Truth True))
															_			->  error ("Arguments for attempt must resolve to boolean: " ++ show z)
				|	otherwise	=	return Nothing


inCharacter :: [Character] -> StateT RpgEnv IO [()]
inCharacter cs = mapM isCharacter cs

inRules :: [Statement] -> StateT RpgEnv IO [()]
inRules rs	=	mapM isRule rs

inStrat :: [Statement] -> StateT RpgEnv IO [[()]]
inStrat ss	=	mapM isCStrat ss

doIt :: [Statement]	-> StateT RpgEnv IO (Maybe Bool)
doIt ss 	=	do
				l<-mapM isAction ss
				return ( case find (\x -> case x of
											Just t 	-> True
											Nothing -> False) l of
							Just q	-> q
							Nothing -> Nothing)

doItWReplace :: Lambda -> [Var] -> StateT RpgEnv IO (Maybe Bool)
doItWReplace (Lambda p b) ss 	= 	do
									env <- get
									let v = (zipWith (\x y -> (x,y)) p ss) in
												do
												--lift $ putStrLn $ "tempVars add: " ++ show v
												--lift $ putStrLn $ "tempVars before: " ++ show (tempVars env)
												put RpgEnv{ rules = rules env, charList = charList env, stratList = stratList env, vars = vars env, lists = lists env, tempVars = v ++ (tempVars env)}
												--lift $ putStrLn $ "tempVars after before: " ++ show (tempVars env)
												g <- doIt b
												env2 <- get
												--lift $ putStrLn $ "tempVars before after: " ++ show (tempVars env)
												put RpgEnv{ rules = rules env, charList = charList env, stratList = stratList env, vars = vars env, lists = lists env, tempVars = drop (length v) (tempVars env2)}
												--lift $ putStrLn $ "tempVars after after: " ++ show (tempVars env)
												--lift $ putStrLn $ show v
												return ( case g of
																Just q	-> Just q
																Nothing -> Nothing)
doStrat		:: Lambda -> String -> StateT RpgEnv IO (Maybe Bool)
doStrat (Lambda p b) s 	=	do
							env <- get
							let l = ( case (snd (head (lists env))) of
										Lst ko	->	if any (\x -> s == x) ko
														then ("allies", snd $ head (lists env)):("enemies", snd ((lists env) !! 1)):[]
														else ("allies", snd ((lists env) !! 1)):("enemies", snd $ head (lists env)):[]
										_		->	error ("You done goofed: " ++ s)) in
								put RpgEnv { rules = rules env, charList = charList env, stratList = stratList env, vars = vars env, lists= l, tempVars = tempVars env}
							g<- doIt b
							return ( case g of
										Just q	-> Just q
										Nothing -> Nothing)



-- Functions for helping with character files:

getName :: [Character] -> String -> [Character]
getName chs name = filter (\s@(Character nam _ _) -> nam == name) chs

getOtherName :: [Character] -> String -> [Character]
getOtherName chs name = filter (\s@(Character nam _ _) -> nam /= name) chs


