module ActionParser where

import UnifiedAST
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.Parsec.Pos (newPos)
import Debug.Trace
import Data.List

data Lex = PLit String | PName String | PNum String | Operator Char | Indent Int | End
     deriving (Show, Eq)

type LParser a = GenParser Lex () a

-- Parser code:

actFile :: LParser Program
actFile = many stmt

stmt :: LParser Statement
stmt = do a <- try while <|> try ifp <|> try for <|> try assign <|> statExpr
          isToken isEnd
          return a

pimport :: LParser Statement
pimport = do isToken $ isWord "import"
             (PName a) <- isToken $ isName
             return $ Import a

block :: LParser Block
block = many stmt

while :: LParser Statement
while = do isToken $ isWord "while"
           a <- expr
           isToken $ isOp ':'
           b <- many stmt
           return $ While a b

ifp :: LParser Statement
ifp = try ifelse <|> ifonly

ifonly :: LParser Statement
ifonly = do
             isToken $ isWord "if"
             a <- expr
             isToken $ isOp ':'
             b <- block
             return $ If a b

ifelse :: LParser Statement
ifelse = do
             isToken $ isWord "if"
             a <- expr
             isToken $ isOp ':'
             b <- block
             isToken isEnd
             isToken $ isWord "else"
             isToken $ isOp ':'
             c <- block
             return $ IfElse a b c

for :: LParser Statement
for = do 
         (PName a) <- between (isToken $ isWord "for") (isToken $ isWord "in") (isToken isName)
         b <- expr
         isToken $ isOp ':'
         c <- block
         return $ For a b c

statExpr :: LParser Statement
statExpr = do a <- expr         -- I have a lot of these two-line do's in the program, find them more readable than the >>= notation
              return $ Exp a

charref :: LParser Expression
charref = do (PName a) <- isToken $ isName
             isToken $ isOp '.'
             (PName b) <- isToken $ isName
             return $ Char a b

prim :: LParser Expression
prim = try paren <|> try charref <|> try litstr <|> try lit <|> var

term :: LParser Expression
term = try list <|> try func <|> prim

litstr :: LParser Expression
litstr = do (PLit a) <- isToken $ isLit
            return $ LitStr a

expr :: LParser Expression
expr = buildExpressionParser table term

table = [[inf (isOp '$') (:$:) AssocLeft],
         [inf (isOp '*') (:*:) AssocLeft, inf (isOp '/') (:/:) AssocLeft],
         [inf (isOp '+') (:+:) AssocLeft, inf (isOp '-') (:-:) AssocLeft, prf (isOp '-') (Negate)],
         [inf (isOp '>') (:>:) AssocNone, inf (isOp '<') (:<:) AssocNone],
         [inf (isWord "or") (:|:) AssocLeft, inf (isWord "and") (:&:) AssocLeft, prf (isWord "not") (Not)]]
        where 
           inf s f assoc = Infix (do{isToken s; return f}) assoc
           prf s f       = Prefix (do{isToken s; return f})        

paren :: LParser Expression
paren = between (isToken $ isOp '(') (isToken $ isOp ')') expr

list :: LParser Expression
list = do a <- between (isToken $ isOp '[') (isToken $ isOp ']') args
          return $ List a
           
func :: LParser Expression
func = do a <- prim
          b <- between (isToken $ isOp '(') (isToken $ isOp ')') args
          return $ Call a b
          
args :: LParser Args
args = do a <- sepBy arg (isToken $ isOp ',')
          return a
           
arg :: LParser Arg
arg = do a <- expr
         return $ PosArg a

var :: LParser Expression
var = do (PName a) <- isToken isName
         return $ Var a
         
lit :: LParser Expression
lit = do (PNum a) <- isToken isNum
         return $ Lit $ toInt a
         
assign :: LParser Statement
assign = do (PName a) <- isToken isName
            isToken $ isOp '='
            b <- expr
            return $ Assign a b

-- Dr. Peterson's isTok method, changed the name so I could import his and my code into GHCi w.o conflicts.
isToken :: (Lex -> Bool) -> LParser Lex
isToken f = token show (\t -> newPos "" 0 0) 
                (\t -> if f t then Just t else Nothing)

isWord :: String -> Lex -> Bool
isWord word (PName wd) = word == wd
isWord _ _ = False

isOp :: Char -> Lex -> Bool
isOp ch (Operator ch1) = ch == ch1
isOp _ _ = False

isName :: Lex -> Bool
isName (PName _) = True
isName _        = False

isNum :: Lex -> Bool
isNum (PNum _) = True
isNum _        = False

isEnd :: Lex -> Bool
isEnd (End) = True
isEnd _ = False

isLit :: Lex -> Bool
isLit (PLit _) = True
isLit _        = False

toInt :: String -> Int
toInt str = read str

parseIt :: String -> Program
parseIt inp = y where (Right y) = let (Right x) = parse srcFile "?" inp 
                                  in parse actFile "?" x

getActions :: Either ParseError [Statement] -> [Statement]
getActions (Right xs) = xs

parseActions :: String -> [Statement]
parseActions inp = let (Right x) = parse srcFile "?" inp in
  getActions $ parse actFile "?" x

-- Lexer code:

srcFile :: GenParser Char st [Lex]
srcFile = do a <- sepBy line eol
             return $ process a

line :: GenParser Char st [Lex]
line = do a <- indent
          b <- many toke
          return (a:b)

process :: [[Lex]] -> [Lex]
process a = convertToEnds $ removeBlLi a

removeBlLi :: [[Lex]] -> [[Lex]]
removeBlLi a = [x | x <- a, case x of [Indent _] -> False
                                      _          -> True]

pname :: GenParser Char st Lex
pname = do a <- letter
           b <- many (letter <|> digit)
           return $ PName (a : b)

num :: GenParser Char st Lex
num = do a <- many1 digit
         return $ PNum a

oper :: GenParser Char st Lex
oper = do a <- oneOf "+-/*=():,<>[].$"
          return $ Operator a

toke :: GenParser Char st Lex
toke =  do a <- try plit <|> try oper <|> try num <|> pname
           skipMany $ char ' '
           return a

plit :: GenParser Char st Lex
plit = do char '"'
          a <- manyTill (anyChar) (char '"')
          return $ PLit a
          
indent :: GenParser Char st Lex
indent = try tabs <|> indents

indents :: GenParser Char st Lex
indents = do a <- many $ char ' '
             return $ (Indent $ length a)

tabs :: GenParser Char st Lex
tabs = do a <- many1 tab
          return $ (Indent $ length a)

convertToEnds :: [[Lex]] -> [Lex]
convertToEnds lst = convertToEndsTup lst (negate 1) []

convertToEndsTup :: [[Lex]] -> Int -> [Int] -> [Lex]
convertToEndsTup [] lvl fors = []
convertToEndsTup [x] _ _    =         tail x ++ [End]
convertToEndsTup (x:xs) lvl fors =         let (Indent indOne) = head x
                                               (Indent indTwo) = head $ head xs
                                               noInd = tail x
                                               lvl1 = if lvl == -1 then indOne else lvl
                                           in if (indTwo > indOne)
                                              then if last x == Operator ':'
                                                   then let (blk, rst) = getBlock indOne xs in tail x ++ convertToEndsTup blk (negate 1) [] ++ (End : convertToEndsTup rst lvl1 fors) --- call this on all following lines farther than this for, stop when indentation <= this statement
                                                   else tail x ++ convertToEndsTup xs lvl1 fors
                                              else if (indTwo == indOne)
                                                   then if (lvl1 /= indOne)
                                                        then tail x ++ convertToEndsTup xs lvl1 fors
                                                        else tail x ++ (End : convertToEndsTup xs lvl1 fors)
                                                   else if (indTwo > lvl1)
                                                        then tail x ++ convertToEndsTup xs lvl1 fors
                                                        else if (not $ null $ fors)
                                                             then if (indTwo <= head fors)
                                                                  then tail x ++ (getEnds fors indTwo) ++ (convertToEndsTup xs (negate 1) (dropWhile (>= indTwo) fors))
                                                                  else undefined
                                                             else tail x ++ (End : convertToEndsTup xs (negate 1) fors)
                                                                                
getBlock :: Int -> [[Lex]] -> ([[Lex]], [[Lex]])
getBlock ind lst = (takeWhile (\x -> let (Indent i) = head x in i > ind) lst, dropWhile (\x -> let (Indent i) = head x in i > ind) lst)
                                                                                
getEnds :: [Int] -> Int -> [Lex]
getEnds fors ind = let oldFors = takeWhile (>= ind) (fors)
                       in replicate (length oldFors + 1) (End)

-- EOL def taken from Real World Haskell. Figured I'd put it in since I'm coding on a Windows machine.
eol =   try (string "\n\r")
    <|> try (string "\r\n")
    <|> string "\n"
    <|> string "\r"
    
-- Test Functions:

testLex :: String -> IO ()
testLex inp = parseTest srcFile inp

testLexF :: String -> IO [Lex]
testLexF inp = do (Right a) <- parseFromFile srcFile inp
                  return a

actParseTest p inp = let (Right a) = parse srcFile "?" inp in parseTest p a

actParseTestF p file = do (Right a) <- parseFromFile srcFile file
                          parseTest p a