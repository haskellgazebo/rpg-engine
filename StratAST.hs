module StratAST where

data Statement
  = NewStrat Name Args Block
  | CStrat Name Block
  |	Assign Name Expression
  | For Name Expression Block
  | Exp Expression
  | If Expression Block
  | IfElse Expression Block Block
  | Import Name
  deriving (Show, Eq)

data Expression
  = Lit Int
  | Var Name
  | Negate Expression
  | Expression :+: Expression
  | Expression :-: Expression
  | Expression :*: Expression
  | Expression :/: Expression
  | Expression :<: Expression
  | Expression :>: Expression
  | Expression :~: Expression
  | Call Expression Args
  | Return Expression
  deriving (Show, Eq)

data Arg
  = NamedArg Name Expression
  | PosArg Expression
  deriving (Show, Eq)

type Name    = String
type Program = [Statement]
type Block   = [Statement]
type Args    = [Arg]