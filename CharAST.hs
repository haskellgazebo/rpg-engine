module CharAST where

data ActorType = ActorType String [String] deriving (Show, Eq)
-- String: Name of the ActorType
-- [String]: List of attributes for that character.

data CharacterType = CharacterType String ActorType [(String, Int)] deriving (Show, Eq)
-- String: CharacterType name
-- ActorType: The ActorType for this CharacterType
-- [(String, Int)]: A list of attributes and their integer values

data Character = Character String CharacterType [(String, Int)] deriving (Show, Eq)
-- String: Character Name
-- CharacterType: The CharacterType for the Character
-- [(String, Int)]: The current attribute values for this Character instance

type AList = [ActorType]
type CTList = [CharacterType]
type CList = [Character]